<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Files;
use App\Models\UserContent;
use App\Models\Folders;
use File;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index(Request $r){
        return view('user.user-dashboard');
    }

    public function loadUploadView(Request $r){
        return view('user.upload-content');
    }

    public function upload(Request $r){

        if($r->hasFile('files')){

            $file = $r->file('files');
            $parent_id = ($r->get('parent_id')!=null) ? $r->get('parent_id') : 0;

            $file_name = $file->getClientOriginalName();
            $file_size = $file->getSize();

            $file_size_status = Files::verifyFileSize($file_size);

            if($file_size_status->original['status']!=1){
                abort(400);
            }

            $extension_array = explode('.',$file_name);
            $extension = end($extension_array);
            $extension = base64_encode($extension);

            //Move Uploaded File
            $destinationPath = public_path() . "/uploads/user_".$r->user()->id.'/';

            Folders::checkRootDir($r->user()->id);

            $file_contents = File::get($file->getPathName());

            $encrypted_file_contents = encrypt($file_contents);

            $file_path = $r->user()->id.'_'.time();

            File::put($destinationPath.$file_path, $encrypted_file_contents);

            Files::create([
                'user_id' => $r->user()->id,
                'folder_id' => $parent_id,
                'file_name' => $file_name,
                'encrypted_name' => $file_path,
                'extension' => $extension
            ]);

            $message = "File uploaded successfully";

        }else{
            $message = "File upload failed";
        }

        // return response()->json($response);
        return redirect()->back()->withMessage([$message]);
    }

    public function downloadFile($file_id,Request $r){

        $file_data = Files::findOrFail($file_id);
        if($file_data->user_id!=$r->user()->id){
            abort(403);
        }

        $directory_path = public_path().'/uploads/user_'.$r->user()->id.'/';

        $file_path = $directory_path.$file_data->encrypted_name;

        if(!File::exists($file_path)){
            abort(404);
        }

        $decoded_extension = $file_data->extension;
        $temp_file = $directory_path.$file_data->file_name;
        $file_contents = File::get($file_path);
        $decrypted_contents = decrypt($file_contents);

        File::put($temp_file, $decrypted_contents);

        return response()->download($temp_file)->deleteFileAfterSend(true);
    }

    public function getUserContent(Request $r){

        $parent_id = $r->get('parent_id');
        $user_data = Folders::getAllFiles($parent_id);

        return response()->json($user_data);
    }

    public function createUserFolder(Request $r){


        $parent_id = ($r->get('parent_id')!=null) ? $r->get('parent_id') : 0;

        if($r->get('folder_name')=="" || $r->get('folder_name')==null){
            return response()->json([
                'status' => 0,
                'message' => 'Invalid Parameters'
            ]);
        }

        Folders::create_user_folder($r);

        $user_data = Folders::getAllFiles($parent_id);

        return response()->json([
            'status' => 1,
            'message' => 'success',
            'user_data' => $user_data
        ]);
    }

    public function updateFileName(Request $r){
        if(($r->get('id')!=null && $r->get('id')!="") && ($r->get('name')!=null && $r->get('name')!="")){
            Files::where('id',$r->get('id'))
            ->update([
                'file_name' => $r->get('name')
            ]);

            return response()->json([
                'file_status' => 1,
                'name' => $r->get('name'),
                'id' => $r->get('id')
            ]);
        }
        else{
            return response()->json([
                'file_status' => 0,
                'name' => 'Invalid parameters'
            ]);            
        }
    }

    public function updateFolderName(Request $r){
        if(($r->get('id')!=null && $r->get('id')!="") && ($r->get('name')!=null && $r->get('name')!="")){
            Folders::where('id',$r->get('id'))
            ->update([
                'name' => $r->get('name')
            ]);

            return response()->json([
                'folder_status' => 1,
                'name' => $r->get('name'),
                'id' => $r->get('id')
            ]);
        }
        else{
            return response()->json([
                'folder_status' => 0,
                'name' => 'Invalid parameters'
            ]);            
        }
    }

    public function deleteFile(Request $r){
        if($r->get('id')!=null && $r->get('id')!=""){

            $file = Files::find($r->get('id'));

            $file_location = 'user_'.$r->user()->id.'/'.$file->encrypted_name;

            Storage::delete($file_location);

            $file->delete();

            return response()->json([
                'status' => 1,
                'id' => $r->get('id')
            ]);
        }
        else{
            return response()->json([
                'status' => 0,
                'name' => 'Invalid parameters'
            ]);            
        }
    }

    public function deleteFolder(Request $r){
        if($r->get('id')!=null && $r->get('id')!=""){

            $folders = Folders::where('parent',$r->get('id'))
            ->where('user_id',$r->user()->id)
            ->get();

            $user_id = $r->user()->id;

            $files = Files::whereIn('folder_id',$folders->pluck('id'))
            ->where('user_id',$r->user()->id)
            ->get()
            ->map(function($i)use($user_id) {
                $file_location = public_path().'/uploads/user_'.$user_id.'/'.$i['encrypted_name'];
                if(File::exists($file_location)){
                    Storage::delete('user_'.$user_id.'/'.$i['encrypted_name']);
                }
                return $i;
            });

            if(!$files->isEmpty()){
                Files::destroy($files->pluck('id'));
            }

            if(!$folders->isEmpty()){
                Folders::destroy($folders->pluck('id'));
            }

            return response()->json([
                'status' => 1,
                'id' => $r->get('id')
            ]);
        }
        else{
            return response()->json([
                'status' => 0,
                'name' => 'Invalid parameters'
            ]);            
        }
    }

    public function downloadFolder(Request $r){

        $folder_id = 1;

        $root_folder_data = Folders::where('user_id',$r->user()->id)
        ->where('id',$folder_id)
        ->get();

        // dd($root_folder_data->pluck('id'));

        $root_folder_name = '/temp_'.time().'_'.$r->user()->id;

        Storage::makeDirectory($root_folder_name,0777, true, true);
        chmod(public_path().'/uploads/'.$root_folder_name,0777);

        $file_data = Folders::getFilesByParent($root_folder_data->pluck('id'));

        Files::saveFiles($root_folder_name,$file_data);

        $folder_data = Folders::getFolderByParent($root_folder_data->pluck('id'));

        foreach ($folder_data as $i => $folder) {
            $new_folder_name = $root_folder_name.'/'.$folder['name'];
            Storage::makeDirectory($new_folder_name ,0777, true, true);
            chmod(public_path().'/uploads/'.$new_folder_name,0777);

            $file_data = Folders::getFilesByParent([$folder['id']]);
            Files::saveFiles($new_folder_name,$file_data);
        }
        // dd($folder_data->pluck('id'));

        // dd($files_data);
    }

    public function test(Request $r){

        $file = Files::find(1);
        dd($file->encrypted_name);
        // Storage::delete('file.jpg');

        // dd(Storage::exists('user_1'));
        // $data = Folders::getAllFiles();
        // dd($data);
    }
}
