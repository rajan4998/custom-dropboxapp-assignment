<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use File;

class Files extends Model
{
    protected $table = "files";

    protected $fillable = [
        'folder_id',
        'file_name',
        'encrypted_name',
        'user_id',
        'extension'
    ];

    public static function verifyFileSize($file_size){

        $max_file_size = $_ENV['MAX_FILE_SIZE'];

        if($file_size <= $max_file_size){
            return response([
                'status' => 1
            ]);
        }
        else{
            return response()->json([
                'status' => 0
            ]);
        }
    }

    public static function saveFiles($root_folder_name,$file_data){
        foreach ($file_data as $i => $file_content) {
            $decoded_extension = $file_content['extension'];
            $temp_file = $root_folder_name.'/'.$file_content['name'];
            $real_location = 'uploads/user_'.Auth::user()->id.'/'.$file_content['encrypted_name'];
            $file_contents = File::get($real_location);
            $decrypted_contents = decrypt($file_contents);
            $file = public_path().'/uploads/'.$temp_file;
            File::put($file, $decrypted_contents);
        }
    }
}
