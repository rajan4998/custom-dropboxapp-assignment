<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Files;
use Auth;

class Folders extends Model
{
    protected $table = "folders";

    protected $fillable = [
        'user_id',
        'name',
        'parent'
    ];

    public static function create_user_folder(Request $r){
        $folder_name = $r->get('folder_name');
        $parent_id = ($r->get('parent_id')!=null) ? $r->get('parent_id') : 0;

        Folders::checkRootDir($r->user()->id);
        Folders::create_folder($r->user()->id,$folder_name,$parent_id,$parent_id);
    }

    public static function checkRootDir($user_id){
        if(!Storage::exists('user_'.$user_id)){
            $root_folder_name = 'user_'.$user_id;
            Storage::makeDirectory($root_folder_name,0777, true, true);
            $full_path = public_path().'/uploads/'.$root_folder_name;
            chmod($full_path, 0777);
        }
    }

    public static function create_folder($user_id,$folder_name,$parent_id,$is_root){
        Folders::create([
            'user_id' => $user_id,
            'name' => $folder_name,
            'parent' => $parent_id
        ]);
    }

    public static function getAllFiles($parent_id=0){

        $all_folders = Folders::getFolderByParent($parent_id);

        $folder_ids = collect([]);

        $folder_ids = ($parent_id==0) ? collect([0]) : collect([$parent_id]);

        $all_files = Folders::getFilesByParent($folder_ids);

        return [
            'folders' => $all_folders->toArray(),
            'files' => $all_files,
        ];
    }

    public static function getFolderByParent($parent_id){
        return Folders::select('id','name','updated_at')
        ->where('user_id',Auth::user()->id)
        ->where('parent',$parent_id)
        ->get()
        ->map(function($i){
            $i['updated_time'] = \Carbon\Carbon::parse($i['updated_at'])->format('d M, Y');
            unset($i['updated_at']);
            return $i;
        });
    }


    public static function getFilesByParent($folder_ids){
        return Files::select('id','file_name','extension','encrypted_name','updated_at')
        ->whereIn('folder_id',$folder_ids)
        ->where('user_id',Auth::user()->id)
        ->get()
        ->map(function($i){
            $i['extension'] = base64_decode($i['extension']);
            $i['name'] = $i['file_name'];
            $i['updated_time'] = \Carbon\Carbon::parse($i['updated_at'])->format('d M, Y');
            unset($i['file_name']);
            unset($i['updated_at']);
            // unset($i['extension']);
            return $i;
        })->toArray();
    }

    public static function saveFolders(){

    }
}
