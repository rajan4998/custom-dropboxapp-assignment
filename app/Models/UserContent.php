<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContent extends Model
{
    protected $table = "user_contents";

    protected $fillable = [
        'user_id',
        'file_name',
        'file_path',
        'checksum',
        'extension'
    ];
}
