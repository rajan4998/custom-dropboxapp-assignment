@extends('layouts.app')

@section('content')
<link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">
<script src="{{ asset('js/angular-1.6.9.min.js') }}"></script>


<div ng-app="Dropbox" ng-controller="DropboxCtrl" ng-init="load()" class="container">
    <div class="row">
        <p>Welcome {{ Auth::user()->name }} to the User Dashboard
            <button class="btn btn-sm btn-primary add-btn" data-toggle="modal" data-target="#myModal">Add</button>
            <button class="btn btn-sm btn-default add-btn" ng-click="getRootDirectoryData()">Home</button>
            <div class="form-group col-md-4">
                <input class="form-control input-sm" ng-model="searchbox" type="text" placeholder="Search">
            </div>
        </p>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Last Updated On</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>

                    <tr ng-repeat="item in folders | filter : searchbox">
                        <th scope="row">@{{ $index+1 }}</th>
                        <td><a href="#" ng-click="getDirectoryData()">@{{ item.name }}</a></td>
                        <td>@{{ item.updated_time }}</td>
                        <td>
                            <a href="#" class="btn-sm btn-primary" data-id="@{{item.id}}" data-toggle="modal" data-target="#editFolder" ng-click="getPostData()" data-type="1">Edit</a>
                            <a href="#" class="btn-sm btn-danger" data-id="@{{item.id}}" data-toggle="modal" data-target="#deleteFolderModal" ng-click="getPostData()">Delete</a>
                        </td>
                    </tr>
                    <tr ng-repeat="item in files | filter : searchbox" class="file_@{{item.id}}">
                        <th scope="row">--</th>
                        <td>@{{ item.name }}</td>
                        <td>@{{ item.updated_time }}</td>
                        <td>
                            <a href="#" class="btn-sm btn-primary" data-id="@{{item.id}}" data-toggle="modal" data-target="#editModal" ng-click="getPostData()">Edit</a>
                            <a href="#" class="btn-sm btn-danger" data-id="@{{item.id}}" data-toggle="modal" data-target="#deleteModal" ng-click="getPostData()">Delete</a>
                            <a class="btn-sm btn-warning" href="download/@{{ item.id }}" target="_blank">Download</a>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>

    <input type="hidden" id="parent_dir" value="0"/>

    <!-- create Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Upload Files</h4>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <ul class="nav nav-tabs">
                        <li class="nav-item active">
                            <a class="nav-link active" data-toggle="tab" href="#create_folder">Create a Folder</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#create_file">Create a File</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div id="create_folder" class="container tab-pane active"><br>
                            <div class="form-group row">
                                <label for="folder_name" class="col-sm-2 col-form-label">Folder Name</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control folder_name" placeholder="Your Folder Name Here" id="folder_name">
                                </div>
                                <div class="col-sm-1">
                                    <a href="#" class="btn btn-primary btn-md float-right" ng-click="addFolder()" data-dismiss="modal">Create</a>
                                </div>
                            </div>
                        </div>
                        <div id="create_file" class="container tab-pane fade"><br>
                            <form class="form-horizontal" method="post" action="{{ url('upload') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Choose a file</label>
                                    <div class="col-sm-3">
                                        <input id="files" name="files" class="input-file" type="file">
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="sbumit" class="btn btn-primary btn-md float-right">Upload</button>
                                    </div>
                                </div>
                                <input type="hidden" id="current_dir" name="parent_id" value="0"/>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- create modal end -->

    <div id="editFolder" class="modal fade" role="dialog" >
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Folder Name: @{{name}}</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <input type="hidden" ng-model="id" value="@{{id}}"/>
                        <input type="hidden" ng-model="type" value="@{{type}}"/>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label pull-right" for="editTitle">Folder Name *</label>
                                </div>
                                <div class="col-md-7">
                                    <input ng-model="name" id="name" name="name" value="@{{name}}" type="text" class="form-control input-md" required>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-primary" ng-click="updateFolder()" data-dismiss="modal">Update</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div id="editModal" class="modal fade" role="dialog" >
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit : @{{name}}</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <input type="hidden" ng-model="id" value="@{{id}}"/>
                        <input type="hidden" ng-model="type" value="@{{type}}"/>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label pull-right" for="editTitle">File Name *</label>
                                </div>
                                <div class="col-md-7">
                                    <input ng-model="name" id="name" name="name" value="@{{name}}" type="text" class="form-control input-md" required>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-primary" ng-click="updateFile()" data-dismiss="modal">Update</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

        <div id="deleteModal" class="modal fade" role="dialog" >
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete File : @{{name}}</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <input type="hidden" ng-model="id" value="@{{id}}"/>
                            <div class="row">
                                <p class="del-modal-txt">Are you sure you want to delete?
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-danger" ng-click="deleteFile()" data-dismiss="modal">Delete</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="deleteFolderModal" class="modal fade" role="dialog" >
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete File : @{{name}}</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <input type="hidden" ng-model="id" value="@{{id}}"/>
                            <div class="row">
                                <p class="del-modal-txt">Are you sure you want to delete?
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-danger" ng-click="deleteFolder()" data-dismiss="modal">Delete</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var app = angular.module('Dropbox',[]);

app.controller('DropboxCtrl',function($scope,$http){

    $scope.load = function(){
        var parent_id = document.getElementById('current_dir').value;
        get_user_data(parent_id);
    }

    $scope.getDirectoryData = function(){
        var parent_id = this.item.id;
        document.getElementById('parent_dir').value = document.getElementById('current_dir').value;
        document.getElementById('current_dir').value = parent_id;
        get_user_data(parent_id);
    }

    $scope.getRootDirectoryData = function(){
        var parent_id = 0;
        document.getElementById('parent_dir').value = 0;
        document.getElementById('current_dir').value = 0;
        get_user_data(parent_id);
    }

    $scope.addFolder = function(){
        var parent_id = document.getElementById('current_dir').value;
        var folder_name = angular.element(document.querySelector('.folder_name')).val();
        $http({
            method : "POST",
            url : "{{ url('/create_folder') }}",
            data : {
                'parent_id' : parent_id,
                'folder_name' : folder_name
            },
        })
        .then(function(response) {
            var folders = [];
            var files = [];
            if(response.data.status===1){
                folders = response.data.user_data.folders;
                if(folders.length > 0){
                    $scope.folders = folders;
                }
                files = response.data.user_data.files;
                if(files.length > 0){
                    $scope.files = files;
                }
            }
            document.getElementById('folder_name').value = "";           
        });
    }

    $scope.getPostData = function(){
        $scope.id = this.item.id;
        $scope.name = this.item.name;
    };

    $scope.updateFolder = function(){
        if(this.name!=""){
            var url = "update_folder_name";
            update_request(this.id,this.name,url);
        }
    }

    $scope.updateFile = function(){
        if(this.name!=""){
            var url = "update_file_name";
            update_request(this.id,this.name,url);
        }
    }

    $scope.deleteFile = function(){

        var url = "delete_file";
        $http({
            method : "POST",
            url : url,
            data : {
                id :this.id
            },
        })
        .then(function(response) {
            angular.element(document.querySelector('.file_'+ response.data.id)).remove();
        });
    }

    $scope.deleteFolder = function(){
        var url = "delete_folder";
        var id = document.getElementById('current_dir').value;
        $http({
            method : "POST",
            url : url,
            data : {
                id :id
            },
        })
        .then(function(response) {
            angular.element(document.querySelector('.file_'+ response.data.id)).remove();
        });

    }

    function update_request(id,name,url){

        $http({
            method : "POST",
            url : url,
            data : {
                id :id,
                name :name,
            },
        })
        .then(function(response) {

            if(response.data.folder_status===1){
                var folder = response.data;
                for(var i=0;i < $scope.folders.length;i++){
                    if(folder.id==$scope.folders[i].id){
                        $scope.folders[i].name= folder.name;
                        break;
                    }
                }
            }

            if(response.data.file_status===1){
                var file = response.data;
                for(var i=0;i < $scope.files.length;i++){
                    if(file.id==$scope.files[i].id){
                        $scope.files[i].name= file.name;
                        break;
                    }
                }
            }

        });
    }

    function get_user_data(parent_id){
        $http({
            method : "POST",
            url : "{{ url('/get_user_data') }}",
            data : {
                "parent_id" : parent_id
            },
        })
        .then(function(response){
            var files = [];
            folders = response.data.folders;
            if(response.data.folders.length > 0){
                $scope.folders = response.data.folders;
            }else{
                $scope.folders = [];
            }
            if(response.data.files.length > 0){
                $scope.files = response.data.files;
            }
            else{
                $scope.files = [];                
            }
        });
    }
});
</script>

@endsection
