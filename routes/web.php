<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    //web app routes
    Route::post('/upload','UserController@upload');
    Route::get('/download/{id}','UserController@downloadFile');
    Route::get('/download_folder/{id}','UserController@downloadFolder');
    Route::get('/','UserController@index');

    //api routes
    Route::post('/get_user_data','UserController@getUserContent');
    Route::post('/create_folder','UserController@createUserFolder');
    Route::post('/update_file_name','UserController@updateFileName');
    Route::post('/update_folder_name','UserController@updateFolderName');
    Route::post('/delete_file','UserController@deleteFile');
    Route::post('/delete_folder','UserController@deleteFolder');
});